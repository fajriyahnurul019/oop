<?php

  require_once('animal.php');
  require_once('frog.php');
  require_once('ape.php');

  $sheep = new Animal("shaun");
  echo "Animal's name : ". $sheep->name."<br>";
  echo "Number of leg : ". $sheep->legs."<br>";
  echo "Cold blooded : ". $sheep->cold_blooded."<br><br>";

  $frog= new Frog("Buduk");
  echo "Animal's name : ". $frog->name."<br>";
  echo "Number of leg : ". $frog->legs."<br>";
  echo "Cold blooded : ". $frog->cold_blooded."<br>";
  echo "Jump : ";
  echo $frog->jump()."<br>";

  $ape= new Ape("Kera sakti");
  echo "Animal's name : ". $ape->name."<br>";
  echo "Number of leg : ". $ape->legs."<br>";
  echo "Cold blooded : ". $ape->cold_blooded."<br>";
  echo "Yell : ";
  echo $ape->yell()."<br>";

?>
